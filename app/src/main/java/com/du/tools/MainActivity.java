package com.du.tools;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.du.lintools.BaseActivity;
import com.du.lintools.ScreenUtils;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showLog(false);
        ScreenUtils.setCustomDensity(this , getApplication());
        log("hello");
    }
}
